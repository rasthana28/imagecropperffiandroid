package com.kone.imageEditor;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
//import android.support.v4.app.ActivityCompat;
import androidx.core.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;


import com.konylabs.android.KonyMain;
import com.konylabs.vm.Function;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

public class CropActivity extends Activity
{
  String path;

  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    getCropIntent();
  }

  public Uri getImageUri(Context inContext, Bitmap inImage)
  {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    inImage.compress(CompressFormat.JPEG, 100, bytes);
    this.path = Media.insertImage(inContext.getContentResolver(), inImage, "croppedImg.jpg", null);
    return Uri.parse(this.path);
  }

  public void getCropIntent()
  {
    try {
      System.out.println("getCropIntent --> START");
      if (isStoragePermissionGranted())
      {
        Bitmap selectededImage = SamplingUtil.handleSamplingAndRotationBitmap(this);
        Uri uri = getImageUri(CropActivity.this, selectededImage);
        Intent CropIntent = new Intent("com.android.camera.action.CROP");
        CropIntent.setDataAndType(uri, "image/*");

        CropIntent.putExtra("crop", "true");
        CropIntent.putExtra("outputX", 800);
        CropIntent.putExtra("outputY", 800);
        CropIntent.putExtra("scale", true);

        CropIntent.putExtra("return-data", true);
        File file = new File(Environment.getExternalStorageDirectory(), "/temporaryImageHold.jpg");
        Uri imgUri = Uri.fromFile(file);
        CropIntent.putExtra("output", imgUri);
        if (CropIntent.resolveActivity(getPackageManager()) != null)
          startActivityForResult(CropIntent, 1);
        else {
          Toast.makeText(this, "Not able to crop", Toast.LENGTH_LONG).show();
        }
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  public void onActivityResult(int requestCode, int resultCode, Intent data)
  {
    System.out.println(resultCode + " onActivityResult --> START with resultCode " + requestCode);
    Bitmap bitmap = null;
    String encodedImage = "";
    try
    {
      if ((requestCode == 1) && 
        (data != null)) { Uri imageUri = data.getData();
        InputStream imageStream = null;
        File file;
        try { imageStream = getContentResolver().openInputStream(imageUri);
          bitmap = BitmapFactory.decodeStream(imageStream);
        } catch (Exception ex) {
          ex.printStackTrace();
          try
          {
            if (imageStream != null)
              imageStream.close();
          }
          catch (IOException localIOException) {
          }
          try {
            File file1 = new File(Environment.getExternalStorageDirectory(), "/temporaryImageHold.jpg");
            if (file1.exists())
              file1.delete();
          } catch (Exception ex1) {
            ex1.printStackTrace();
          }
        }
        finally
        {
          try
          {
            if (imageStream != null)
              imageStream.close();
          }
          catch (IOException localIOException1) {
          }
          try {
            File file2 = new File(Environment.getExternalStorageDirectory(), "/temporaryImageHold.jpg");
            if (file2.exists())
              file2.delete();
          } catch (Exception ex) {
            ex.printStackTrace();
          }

        }

        encodedImage = encodeImage(bitmap);
      }
      if(Crop.callback!=null) {
        Crop.callback.execute(new Object[]{
                encodedImage});
      } else {
        Log.e("CropActivity","The callback is null");
      }
    }
    catch (Exception e)
    {
      System.out.println("inside callback catch");
      e.printStackTrace();
      try {
        Crop.callback.execute(new Object[] { 
          e });
      } catch (Exception e1) {
        e1.printStackTrace();
      }
    }
    finish();
  }

  private String encodeImage(Bitmap bm) {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    bm.compress(CompressFormat.JPEG, 50, baos);
    byte[] b = baos.toByteArray();
    String encImage = Base64.encodeToString(b, 0);

    return encImage;
  }

  public boolean isStoragePermissionGranted() {
    if (VERSION.SDK_INT >= 23)
    {
      if (checkSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") == PackageManager.PERMISSION_GRANTED)
      {
        return true;
      }
      ActivityCompat.requestPermissions(this, new String[] { "android.permission.WRITE_EXTERNAL_STORAGE" }, 1);
      return false;
    }
    return true;
  }

  public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
  {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    if (grantResults[0] == 0)
    {
      System.out.println("Permission: " + permissions[0] + "was " + grantResults[0]);

      getCropIntent();
    }
  }
}