package com.kone.imageEditor;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.IOException;

public class SamplingUtil
{
  public static Bitmap handleSamplingAndRotationBitmap(Context context)
    throws IOException
  {
    Options options = new Options();
    options.inJustDecodeBounds = true;
    byte[] imageAsBytes = Base64.decode(Crop.imageName.getBytes(), 0);
    Bitmap selectededImage = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    
    String path = Media.insertImage(context.getContentResolver(), selectededImage, "croppedImg.jpg", null);
    Bitmap img = rotateImageIfRequired(context, selectededImage, Uri.parse(path), imageAsBytes);
    return img;
  }
  
  private static Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage, byte[] imageAsBytes)
    throws IOException
  {
    ExifInterface ei;
    if (VERSION.SDK_INT > 23) {
      ei = new ExifInterface(new ByteArrayInputStream(imageAsBytes));
    } else {
      ei = new ExifInterface(selectedImage.getPath());
    }
    int orientation = ei.getAttributeInt("Orientation", 1);
    switch (orientation)
    {
    case 6: 
      return rotateImage(img, 90);
    case 3: 
      return rotateImage(img, 180);
    case 8: 
      return rotateImage(img, 270);
    }
    return img;
  }
  
  private static Bitmap rotateImage(Bitmap img, int degree)
  {
    Matrix matrix = new Matrix();
    matrix.postRotate(degree);
    Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
    img.recycle();
    return rotatedImg;
  }
}
