package com.kone.imageEditor;

import android.content.Intent;
import com.konylabs.android.KonyMain;
import com.konylabs.vm.Function;
import java.io.PrintStream;

public class Crop
{
  public static Function callback = null;
  public static KonyMain konyContext;
  public static String imageName;
  
  public static void getCropImage(String imageNameVal, Function cb)
  {
    System.out.println("getCropImage --> START");
    imageName = imageNameVal;
    callback = cb;
    konyContext = KonyMain.getActivityContext();
    Intent cropIntentValue = new Intent(konyContext, CropActivity.class);
    konyContext.startActivity(cropIntentValue);
    System.out.println("getCropImage --> END");
  }

}
